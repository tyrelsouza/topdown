var game = new Phaser.Game(1152, 850, Phaser.AUTO, '');
var player;

// http://talestolduntold.blogspot.com/2015/06/tilemaps-with-invisible-collision-layer.html

var TREE_IDS = [
  181, 182, 208, 209, 183, // Forest
  184, 185, 211, 212, 186, // Fall
];

game.state.add('play', {
  preload: function(){
    game.load.spritesheet('character', 'assets/arrow_tileset.png', 32, 32);
    game.stage.backgroundColor = '#787878';

    // load map data
    game.load.tilemap('forest1', 'assets/tilemaps/maps/Forest1.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('forest2', 'assets/tilemaps/maps/Forest2.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('forest3', 'assets/tilemaps/maps/Forest3.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('fall1', 'assets/tilemaps/maps/Fall1.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('fall2', 'assets/tilemaps/maps/Fall2.json', null, Phaser.Tilemap.TILED_JSON);

    //load tilesheet data
    game.load.image('tiles', 'assets/tilemaps/tiles/tilesheet_complete.png');
    this.layers = {};
  },
  create: function(){
    // enable p2physics
    game.physics.startSystem(Phaser.Physics.P2JS);

    // use cursors for movement
    this.cursors = this.game.input.keyboard.createCursorKeys();

    //make a layer
    this.createLayers('forest1');
    this.createPlayer();
    game.physics.p2.enable(this.player)
  },
  update: function(){
    this.playerMovement(); // move player

    // check for collider, how does this work, i'm not using arcade?
    game.physics.arcade.collide(player, this.layers['trees']);

    game.debug.bodyInfo(player, 32, this.game.world.height - 100);

  },

  /*  CUSTOM METHODS VVVVV */
  createLayers: function(map_name){
    // load the tilemap
    this.map = game.add.tilemap(map_name);
    // apply the tileset
    this.map.addTilesetImage('kenney_complete', 'tiles');

    // create all layers
    this.layers['water'] = this.map.createLayer('Water');
    this.layers['ground'] = this.map.createLayer('Ground');
    this.layers['trees'] = this.map.createLayer('Trees');
    this.layers['misc'] = this.map.createLayer('Misc');

    // collisions between trees
    game.physics.p2.convertTilemap(this.map, "Trees");
    for (var i in TREE_IDS){
      this.map.setCollision(TREE_IDS[i], true, "Trees");
    }
  },
  createPlayer: function(){
    // create the character, enble physics on it, and add animations
    player = game.add.sprite(80, 144, 'character');
    game.physics.enable(player);

    player.body.collideWorldBounds = true;
    player.animations.add('up',    [9,10,11], 10, true);
    player.animations.add('down',  [0,1,2],   10, true);
    player.animations.add('left',  [3,4,5],   10, true);
    player.animations.add('right', [6,7,8],   10, true);
    player.speed = 200;


  },
  playerMovement: function(){
    // zero out velocity
    player.body.velocity.y = 0;
    player.body.velocity.x = 0;

    // standard move and animate code.
    if(this.cursors.up.isDown) {
      player.body.velocity.y -= player.speed;
      player.animations.play('up');
    }
    else if(this.cursors.down.isDown) {
      player.body.velocity.y += player.speed;
      player.animations.play('down');
    }
    if(this.cursors.left.isDown) {
      player.body.velocity.x -= player.speed;
      player.animations.play('left');
    }
    else if(this.cursors.right.isDown) {
      player.body.velocity.x += player.speed;
      player.animations.play('right');
    } else {
     player.animations.stop(null, true);
    }
  }




});


game.state.start('play');


